<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div align="center" dir="auto">
  <a href="https://wasmer.io" rel="nofollow">
    <themed-picture data-catalyst-inline="true" data-catalyst=""><picture>
      <source srcset="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/logo-white.png" media="(prefers-color-scheme: dark)">
      <img width="300" src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/logo.png" alt="瓦斯默标志" style="visibility:visible;max-width:100%;">
    </picture></themed-picture>
  </a>
  <p dir="auto">
    <a href="https://github.com/wasmerio/wasmer/actions?query=workflow%3Abuild">
      <img src="https://github.com/wasmerio/wasmer/actions/workflows/build.yml/badge.svg?event=push" alt="构建状态" style="max-width: 100%;">
    </a>
    <a href="https://github.com/wasmerio/wasmer/blob/main/LICENSE">
      <img src="https://camo.githubusercontent.com/3939383be1837d38d1654ea0980a267646249a0a97366e57a4f0d579388b2bc6/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6c6963656e73652f7761736d6572696f2f7761736d65722e737667" alt="执照" data-canonical-src="https://img.shields.io/github/license/wasmerio/wasmer.svg" style="max-width: 100%;">
    </a>
    <a href="https://docs.wasmer.io" rel="nofollow">
      <img src="https://camo.githubusercontent.com/f01d51cd11632dd79f98c3cb5ba714769d3d979aa6650404151434d185c746c1/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f6c6162656c3d446f6373266d6573736167653d646f63732e7761736d65722e696f26636f6c6f723d626c7565" alt="瓦斯默文档" data-canonical-src="https://img.shields.io/static/v1?label=Docs&amp;message=docs.wasmer.io&amp;color=blue" style="max-width: 100%;">
    </a>
    <a href="https://discord.gg/rWkMNStrEW" rel="nofollow">
      <img src="https://camo.githubusercontent.com/7493dacf42dca32d8b534916c488f280eb0dc5bfdc154ae81955f30490f739ac/68747470733a2f2f696d672e736869656c64732e696f2f646973636f72642f31313130333030353036393432383831383733" alt="瓦斯默在 Discord 上" data-canonical-src="https://img.shields.io/discord/1110300506942881873" style="max-width: 100%;">
    </a>
  </p>
</div>
<br>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Wasmer 是一个</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">极快</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">且</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全的</font></font></em> <a href="https://webassembly.org" rel="nofollow"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebAssembly</font></font></strong></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">运行时，它使极其
</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">轻量级的容器</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">能够在任何地方运行：从</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">桌面</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">到</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">云端</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">边缘</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和浏览器。</font></font></p>
<ul dir="auto">
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">默认情况下安全</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。除非明确启用，否则无文件、网络或环境访问权限。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可插拔</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">开箱即用地</font><font style="vertical-align: inherit;">支持</font></font><a href="https://wasix.org/" rel="nofollow"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WASIX</font></font></strong></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://github.com/WebAssembly/WASI"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WASI</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://emscripten.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Emscripten 。</font></font></a><font style="vertical-align: inherit;"></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快得令人难以置信</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。以接近本机的速度运行 WebAssembly。</font></font></li>
<li><strong><font style="vertical-align: inherit;"></font></strong> <font style="vertical-align: inherit;"><a href="https://github.com/wasmerio/wasmer/#wasmer-sdk"><font style="vertical-align: inherit;">可通过 Wasmer SDK</font></a><strong><font style="vertical-align: inherit;">嵌入任何地方</font></strong></font><a href="https://github.com/wasmerio/wasmer/#wasmer-sdk"><font style="vertical-align: inherit;"></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装 Wasmer</font></font></h3><a id="user-content-install-wasmer" class="anchor" aria-label="永久链接：安装 Wasmer" href="#install-wasmer"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>curl https://get.wasmer.io -sSfL <span class="pl-k">|</span> sh</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="curl https://get.wasmer.io -sSfL | sh" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<details>
  <summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他安装选项（Powershell、Brew、Cargo...）</font></font></summary>
<p dir="auto"><em>Wasmer can be installed from various package managers. Choose the one that fits best for your environment:</em></p>
<ul dir="auto">
<li>Powershell (Windows)
<div class="highlight highlight-source-powershell notranslate position-relative overflow-auto" dir="auto"><pre>iwr https:<span class="pl-k">//</span>win.wasmer.io <span class="pl-k">-</span>useb <span class="pl-k">|</span> iex</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="iwr https://win.wasmer.io -useb | iex" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</li>
</ul>
<ul dir="auto">
<li>
<p dir="auto"><a href="https://formulae.brew.sh/formula/wasmer" rel="nofollow">Homebrew</a> (macOS, Linux)</p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>brew install wasmer</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="brew install wasmer" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</li>
<li>
<p dir="auto"><a href="https://github.com/ScoopInstaller/Main/blob/master/bucket/wasmer.json">Scoop</a> (Windows)</p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>scoop install wasmer</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="scoop install wasmer" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</li>
<li>
<p dir="auto"><a href="https://chocolatey.org/packages/wasmer" rel="nofollow">Chocolatey</a> (Windows)</p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>choco install wasmer</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="choco install wasmer" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</li>
<li>
<p dir="auto"><a href="https://crates.io/crates/cargo-binstall/" rel="nofollow">Cargo binstall</a></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>cargo binstall wasmer-cli</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="cargo binstall wasmer-cli" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</li>
<li>
<p dir="auto"><a href="https://crates.io/crates/wasmer-cli/" rel="nofollow">Cargo</a></p>
<p dir="auto"><em>Note: All the available
features are described in the <a href="https://github.com/wasmerio/wasmer/tree/main/lib/cli/README.md"><code>wasmer-cli</code>
crate docs</a></em></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>cargo install wasmer-cli</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="cargo install wasmer-cli" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</li>
</ul>
<blockquote>
<p dir="auto">Looking for more installation options? See <a href="https://github.com/wasmerio/wasmer-install">the <code>wasmer-install</code>
repository</a> to learn
more!</p>
</blockquote>
</details>
<blockquote>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注意：您还可以在</font></font><a href="https://wasmer.sh/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">wasmer.sh中在线尝试 Wasmer</font></font></a></p>
</blockquote>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速开始</font></font></h3><a id="user-content-quickstart" class="anchor" aria-label="永久链接：快速入门" href="#quickstart"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://wasmer.io/syrusakbary/cowsay" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以从运行Cowsay</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开始
</font><font style="vertical-align: inherit;">：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>$ wasmer run cowsay <span class="pl-s"><span class="pl-pds">"</span>hello world<span class="pl-pds">"</span></span>
 _____________
<span class="pl-k">&lt;</span> hello world <span class="pl-k">&gt;</span>
 -------------
        <span class="pl-cce">\ </span>  ^__^
         <span class="pl-cce">\ </span> (oo)<span class="pl-cce">\_</span>______
            (__)<span class="pl-cce">\ </span>      )<span class="pl-cce">\/</span>\
               <span class="pl-k">||</span>----w <span class="pl-k">|</span>
                <span class="pl-k">||</span>     <span class="pl-k">||</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="$ wasmer run cowsay &quot;hello world&quot;
 _____________
< hello world >
 -------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
               ||----w |
                ||     ||" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<blockquote>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">还有更多可用的软件包，例如</font></font><a href="https://wasmer.io/wasmer/python" rel="nofollow"><code>wasmer/python</code></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或</font></font><a href="https://wasmer.io/saghul/quickjs" rel="nofollow"><code>quickjs</code></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font><a href="https://docs.wasmer.io/registry/get-started" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创建您自己的包</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，或探索社区中的包：</font></font><a href="https://wasmer.io/explore" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://wasmer.io/explore</font></font></a></p>
</blockquote>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">接下来您可以执行以下操作：</font></font></h4><a id="user-content-here-is-what-you-can-do-next" class="anchor" aria-label="永久链接：接下来您可以执行以下操作：" href="#here-is-what-you-can-do-next"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://docs.wasmer.io/runtime/get-started" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">运行一个包</font></font></a></li>
<li><a href="https://docs.wasmer.io/registry/get-started" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发布包</font></font></a></li>
<li><a href="https://docs.wasmer.io/edge/get-started" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部署您的网站</font></font></a></li>
<li><a href="https://wasmer.io/posts" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解有关瓦斯默的更多信息</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">瓦斯默SDK</font></font></h2><a id="user-content-wasmer-sdk" class="anchor" aria-label="永久链接：Wasmer SDK" href="#wasmer-sdk"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以通过 Wasmer SDK使用</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">嵌入不同语言的</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Wasmer 运行时：</font></font></p>
<table>
<thead>
<tr>
<th></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">包裹</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/rust.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/rust.svg" alt="铁锈标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer/tree/main/lib/api"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">锈</font></font></strong></a></td>
<td><a href="https://crates.io/crates/wasmer/" rel="nofollow"><code>wasmer</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生锈的箱子</font></font></a></td>
<td><a href="https://docs.rs/wasmer/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/c.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/c.svg" alt="C标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer/tree/main/lib/c-api"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">C</font></font></strong></a></td>
<td><a href="https://github.com/wasmerio/wasmer/blob/main/lib/c-api/tests/wasm-c-api/include/wasm.h"><code>wasm.h</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">标头</font></font></a></td>
<td><a href="https://docs.rs/wasmer-c-api/*/wasmer/wasm_c_api/index.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/cpp.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/cpp.svg" alt="C++ 标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer/tree/main/lib/c-api"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">C++</font></font></strong></a></td>
<td><a href="https://github.com/wasmerio/wasmer/blob/main/lib/c-api/tests/wasm-c-api/include/wasm.hh"><code>wasm.hh</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">标头</font></font></a></td>
<td><a href="https://docs.rs/wasmer-c-api/*/wasmer/wasm_c_api/index.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/csharp.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/csharp.svg" alt="C# 标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/migueldeicaza/WasmerSharp"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">C＃</font></font></strong></a></td>
<td><a href="https://www.nuget.org/packages/WasmerSharp/" rel="nofollow"><code>WasmerSharp</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NuGet包</font></font></a></td>
<td><a href="https://migueldeicaza.github.io/WasmerSharp/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/d.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/d.svg" alt="D 标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/chances/wasmer-d"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">D</font></font></strong></a></td>
<td><a href="https://code.dlang.org/packages/wasmer" rel="nofollow"><code>wasmer</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">配音包</font></font></a></td>
<td><a href="https://chances.github.io/wasmer-d" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/python.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/python.svg" alt="蟒蛇标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer-python"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python</font></font></strong></a></td>
<td><a href="https://pypi.org/project/wasmer/" rel="nofollow"><code>wasmer</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyPI包</font></font></a></td>
<td><a href="https://wasmerio.github.io/wasmer-python/api/wasmer" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/js.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/js.svg" alt="JS标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer-js"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">JavaScript</font></font></strong></a></td>
<td><a href="https://www.npmjs.com/org/wasmer" rel="nofollow"><code>@wasmerio</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NPM 包</font></font></a></td>
<td><a href="https://docs.wasmer.io/integrations/js/reference-api" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/go.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/go.svg" alt="去标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer-go"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">去</font></font></strong></a></td>
<td><a href="https://pkg.go.dev/github.com/wasmerio/wasmer-go/wasmer" rel="nofollow"><code>wasmer</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">走包</font></font></a></td>
<td><a href="https://pkg.go.dev/github.com/wasmerio/wasmer-go/wasmer?tab=doc" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/php.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/php.svg" alt="PHP 徽标" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer-php"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PHP</font></font></strong></a></td>
<td><a href="https://pecl.php.net/package/wasm" rel="nofollow"><code>wasm</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PECL封装</font></font></a></td>
<td><a href="https://wasmerio.github.io/wasmer-php/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/ruby.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/ruby.svg" alt="红宝石标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer-ruby"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">红宝石</font></font></strong></a></td>
<td><a href="https://rubygems.org/gems/wasmer" rel="nofollow"><code>wasmer</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">红宝石</font></font></a></td>
<td><a href="https://wasmerio.github.io/wasmer-ruby/wasmer_ruby/index.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/java.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/java.svg" alt="爪哇标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer-java"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">爪哇</font></font></strong></a></td>
<td><a href="https://bintray.com/wasmer/wasmer-jni/wasmer-jni" rel="nofollow"><code>wasmer/wasmer-jni</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">二进制托盘包</font></font></a></td>
<td><a href="https://github.com/wasmerio/wasmer-java/#api-of-the-wasmer-library"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/r.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/r.svg" alt="R 标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/dirkschumacher/wasmr"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">右</font></font></strong></a></td>
<td><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没有发布包</font></font></em></td>
<td><a href="https://github.com/dirkschumacher/wasmr#example"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/postgres.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/postgres.svg" alt="Postgres 徽标" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer-postgres"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Postgres</font></font></strong></a></td>
<td><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没有发布包</font></font></em></td>
<td><a href="https://github.com/wasmerio/wasmer-postgres#usage--documentation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/swift.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/swift.svg" alt="斯威夫特标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/AlwaysRightInstitute/SwiftyWasmer"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迅速</font></font></strong></a></td>
<td><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没有发布包</font></font></em></td>
<td></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/ziglang/logo/master/zig-favicon.png"><img src="https://raw.githubusercontent.com/ziglang/logo/master/zig-favicon.png" alt="之字形标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/zigwasm/wasmer-zig"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">之字形</font></font></strong></a></td>
<td><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没有发布包</font></font></em></td>
<td></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/dart.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/dart.svg" alt="飞镖标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/dart-lang/wasm"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">镖</font></font></strong></a></td>
<td><a href="https://pub.dev/packages/wasm" rel="nofollow"><code>wasm</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">酒吧套餐</font></font></a></td>
<td></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/crystal.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/crystal.svg" alt="水晶标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/naqvis/wasmer-crystal"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">水晶</font></font></strong></a></td>
<td><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没有发布包</font></font></em></td>
<td><a href="https://naqvis.github.io/wasmer-crystal/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习</font></font></a></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/lisp.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/lisp.svg" alt="Lisp 标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/helmutkian/cl-wasm-runtime"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">口齿不清</font></font></strong></a></td>
<td><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没有发布包</font></font></em></td>
<td></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/julia.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/julia.svg" alt="朱莉娅标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/Pangoraw/Wasmer.jl"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">朱莉娅</font></font></strong></a></td>
<td><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没有发布包</font></font></em></td>
<td></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/vlang.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/vlang.svg" alt="VLang标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/vlang/wasmer"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">V</font></font></strong></a></td>
<td><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">没有发布包</font></font></em></td>
<td></td>
</tr>
<tr>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/ocaml.svg"><img src="https://raw.githubusercontent.com/wasmerio/wasmer/master/assets/languages/ocaml.svg" alt="奥卡姆标志" style="max-width: 100%;"></a></td>
<td><a href="https://github.com/wasmerio/wasmer-ocaml"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">奥卡米尔</font></font></strong></a></td>
<td><a href="https://opam.ocaml.org/packages/wasmer/" rel="nofollow"><code>wasmer</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OCaml包</font></font></a></td>
<td></td>
</tr>
</tbody>
</table>
<p dir="auto"><a href="https://github.com/wasmerio/wasmer/issues/new?assignees=&amp;labels=%F0%9F%8E%89+enhancement&amp;template=---feature-request.md&amp;title="><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">👋 缺少语言？</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献</font></font></h2><a id="user-content-contribute" class="anchor" aria-label="永久链接：贡献" href="#contribute"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们感谢您的帮助！ 💜</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们建议阅读以下有关如何成功参与复杂项目的指南：
 </font></font><a href="https://mitchellh.com/writing/contributing-to-complex-projects" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://mitchellh.com/writing/contributing-to-complex-projects</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查看我们的文档，了解如何</font></font><a href="https://docs.wasmer.io/developers/build-from-source" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从源代码构建 Wasmer</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或</font></font><a href="https://docs.wasmer.io/developers/testing" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">测试您的更改</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>

<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">社区</font></font></h2><a id="user-content-community" class="anchor" aria-label="永久链接：社区" href="#community"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Wasmer 拥有一个令人惊叹的开发者和贡献者社区。欢迎您，请加入我们！ 👋</font></font></p>
<ul dir="auto">
<li><a href="https://discord.gg/rWkMNStrEW" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Wasmer 社区不和谐</font></font></a></li>
<li><a href="https://twitter.com/wasmerio" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">瓦斯默在推特上</font></font></a></li>
</ul>
<hr>
<blockquote>
<p dir="auto"><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自述文件还有：
 </font></font><a href="https://github.com/wasmerio/wasmer/blob/main/docs/cn/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🇨🇳 中文 - 中文</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">•
 </font></font><a href="https://github.com/wasmerio/wasmer/blob/main/docs/de/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🇩🇪 德语 - 德语</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">•
 </font></font><a href="https://github.com/wasmerio/wasmer/blob/main/docs/es/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🇪🇸 西班牙语 - 西班牙语</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">•
 </font></font><a href="https://github.com/wasmerio/wasmer/blob/main/docs/fr/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🇫🇷 法语 - 法语</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">•
 </font></font><a href="https://github.com/wasmerio/wasmer/blob/main/docs/ja/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🇯🇵 日本语 - 日语</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">•
 </font></font><a href="https://github.com/wasmerio/wasmer/blob/main/docs/ko/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🇰🇷 한국어 - 韩语</font></font></a></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
</blockquote>
</article></div>
